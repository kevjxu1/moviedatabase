-- Generate the tables

CREATE TABLE Movie (
       id INT,
       title VARCHAR(100),
       `year` INT,
       rating VARCHAR(10),
       company VARCHAR(50),
       PRIMARY KEY (id) -- Every Movie ID is unique       
) ENGINE = INNODB;

CREATE TABLE Actor (
       id INT, PRIMARY KEY (id), -- Actor ID is unique
       `last` VARCHAR(20),
       `first` VARCHAR(20),

       -- There are technically only two sexes
       sex VARCHAR(6), CHECK (sex = 'Male' OR sex = 'Female'), 
       dob DATE, -- date of birth
       dod DATE, -- date of death
       CHECK (dob <= dod) -- date of birth must not be after date of death
) ENGINE = INNODB;

CREATE TABLE Sales (
       mid INT,
       ticketsSold INT,
       totalIncome INT,
       PRIMARY KEY (mid), -- Every Movie ID should appear exactly 0 or 1 time.
       FOREIGN KEY (mid) REFERENCES Movie(id), -- mid is a unique Movie ID
       CHECK (ticketsSold >= 0 AND totalIncome >= 0) -- must be nonnegative   
) ENGINE = INNODB;

CREATE TABLE Director (
       id INT, PRIMARY KEY (id), -- Every Director ID is unique
       `last` VARCHAR(20),
       `first` VARCHAR(20),
       dob DATE, -- date of birth
       dod DATE, -- date of death
       CHECK (dob <= dod) -- date of birth must not be after date of death   
) ENGINE = INNODB;

CREATE TABLE MovieGenre (
       mid INT, -- Movie ID
       genre VARCHAR(20)       
) ENGINE = INNODB;

CREATE TABLE MovieDirector (
       mid INT, -- Movie ID
       did INT, -- Director ID
       FOREIGN KEY (mid) REFERENCES Movie(id), -- mid references a Movie ID
       FOREIGN KEY (did) REFERENCES Director(id) -- did " " Director ID       
) ENGINE = INNODB;

CREATE TABLE MovieActor (
       mid INT, -- Movie ID
       aid INT, -- Actor ID
       `role` VARCHAR(50), -- Actor role in movie
       FOREIGN KEY (mid) REFERENCES Movie(id), -- mid refers to Movie ID
       FOREIGN KEY (aid) REFERENCES Actor(id) -- aid refers to Actor ID       
) ENGINE = INNODB;

CREATE TABLE MovieRating (
       mid INT,
       imdb INT, -- IMDb rating
       rot INT, -- Rotten Tomaoes rating
       PRIMARY KEY (mid), -- Movie ID is unique
       FOREIGN KEY (mid) REFERENCES Movie(id) -- mid refers to a Movie ID  
) ENGINE = INNODB;

-- The following are for part B

CREATE TABLE Review (
       `name` VARCHAR(20), -- Reviewer name
       `time` TIMESTAMP, -- Review time
       mid INT,
       rating INT,
       comment VARCHAR(500)
) ENGINE = INNODB;       

-- The following are for part C

CREATE TABLE MaxPersonID (
       id INT       
) ENGINE = INNODB;

CREATE TABLE MaxMovieID (
       id INT       
) ENGINE = INNODB;

