<!DOCTYPE html>
<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

    if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<FORM ACTION="movies_home.php">
    <INPUT TYPE="submit" VALUE="return to main menu"><br><br>
</FORM>

<b>show movie info</b><br><br>

<?php
	$movie_query = "SELECT id, CONCAT(`title`, '; ', `year`) AS cat FROM Movie ORDER BY cat;";
	$movie_result = mysql_query($movie_query);
?>

<FORM ACTION="movie_info.php" METHOD="post">
	movie:
	<SELECT NAME="movie">
		<OPTION SELECTED VALUE=""></OPTION>
	<?php while ($row = mysql_fetch_row($movie_result)) : ?>
		<OPTION VALUE="<?= $row[0] ?>"><?= $row[1] ?></OPTION>
	<?php endwhile ?>
	</SELECT><br>
	
	<INPUT TYPE="submit" NAME="submit" VALUE="show info"><br><br>	

</FORM>

<?php
	// from form
	$mid = $_POST['movie'];
	if (isset($_GET['linked_mid'])) {
		$mid = $_GET['linked_mid'];
	}

	if (isset($mid)) {
		// get movie title
		$title_result = mysql_query(sprintf("SELECT CONCAT(`title`, '; ', `year`) FROM Movie WHERE id = %d",
		mysql_real_escape_string($mid)));
		$row = mysql_fetch_row($title_result);
		echo "<b>movie: </b>".$row[0]."<br><br>";
	
		// show director(s)
		echo "<b>director(s):</b><br>";
		$select_cmd = sprintf(
			"SELECT CONCAT(`first`, ' ', `last`) FROM MovieDirector, Director WHERE did = id AND mid = %d;",
			mysql_real_escape_string($mid) );
		$result = mysql_query($select_cmd);
		if ($result == FALSE) {
			echo mysql_error();
			exit;
		}
		while ($row = mysql_fetch_row($result)) {
			echo $row[0]."<br>";
		}
		echo "<br>";

		// show actors
		echo "<b>actors:</b><br>";
		$select_cmd = sprintf(
			"SELECT CONCAT(`first`, ' ', `last`) FROM MovieActor, Actor WHERE aid = id AND mid = %d;",
			mysql_real_escape_string($mid) );
		$result = mysql_query($select_cmd);
		if ($result == FALSE) {
			echo mysql_error(); 
			exit;
		}
		while ($row = mysql_fetch_row($result)) {
			echo $row[0];
			echo "<br>";
		}
		echo "<br>";
		
		// show average score
		echo "<b>average score: </b>";
		$select_cmd = sprintf(
			"SELECT AVG(rating) FROM Review WHERE mid = %d GROUP BY mid;",
			mysql_real_escape_string($mid) );
		$result = mysql_query($select_cmd);
		if ($result == FALSE) {
			echo mysql_error();
			exit;
		}
		$row = mysql_fetch_row($result);
		echo $row[0]."<br><br>";
?>

<!-- add comment -->
<FORM ACTION="add_movie_comment.php" METHOD="post">
	<INPUT TYPE="submit" VALUE="add a review"><br><br>
</FORM>

<?php		
		// show comments
		echo "<b>comments</b><br>";
		$select_cmd = sprintf(
			"SELECT name, time, rating, comment FROM Review WHERE mid = %d",
			mysql_real_escape_string($mid) );
		$result = mysql_query($select_cmd);
		while ($row = mysql_fetch_row($result)) {
			echo "<b>name:</b>&nbsp;".$row[0]."<br>";
			echo "<b>timestamp:</b>&nbsp;".$row[1]."<br>";
			echo "<b>rating:</b>&nbsp;".$row[2]."<br>";
			echo "<b>comment:</b>&nbsp;".$row[3]."<br>";
			echo "<br>";
		}
	}
	else {
		echo "press 'show info' when ready"."<br>";
	}	
?>

