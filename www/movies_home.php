<!DOCTYPE html>
<h1>MAIN MENU</h1>
What do you want to do?<br><br>

<FORM ACTION="add_actor_or_director.php">
	<INPUT TYPE="submit" VALUE="add actor or director"><br>
</FORM>

<FORM ACTION="add_movie.php">
	<INPUT TYPE="submit" VALUE="add movie">
</FORM>

<FORM ACTION="add_movie_comment.php">
	<INPUT TYPE="submit" VALUE="add movie comment">
</FORM>

<FORM ACTION="add_actor_to_movie.php">
	<INPUT TYPE="submit" VALUE="add actor to movie"><br>
</FORM>

<FORM ACTION="add_director_to_movie.php">
	<INPUT TYPE="submit" VALUE="add director to movie"><br>
</FORM>

<FORM ACTION="actor_info.php">
	<INPUT TYPE="submit" VALUE="view actor info"><br>
</FORM>

<FORM ACTION="movie_info.php">
	<INPUT TYPE="submit" VALUE="view movie info"><br><br>
</FORM>

<FORM ACTION="movies_home.php" METHOD="post">
	search for an actor:<br>
	<INPUT TYPE="text" NAME="actor_search" SIZE=30 MAXLENGTH=41>
	<INPUT TYPE="submit" NAME="actor_submit" VALUE="search"><br>
</FORM>

<FORM ACTION="movies_home.php" METHOD="post">
	search for a movie:<br>
	<INPUT TYPE="text" NAME="movie_search" SIZE=50 MAXLENGTH=100>
	<INPUT TYPE="submit" NAME="movie_submit" VALUE="search"><br><br>
</FORM>

<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

    if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<?php
	$actor_substr = $_POST['actor_search'];
	$movie_substr = $_POST['movie_search'];
	$actor_submitted = isset($_POST['actor_submit']);
	$movie_submitted = isset($_POST['movie_submit']);

	if ($actor_submitted) {
		/*$query = "SELECT id, CONCAT(`first`, ' ', `last`) FROM Actor WHERE `first` LIKE '%".sprintf("%s", mysql_real_escape_string($actor_substr))."%' OR `last` like '%".sprintf("%s", mysql_real_escape_string($actor_substr))."%';";*/

		$query = "SELECT id, actor FROM (SELECT CONCAT(`first`, ' ', `last`, '; ', dob) AS actor, id FROM Actor) AS S WHERE actor LIKE '%".sprintf("%s", mysql_real_escape_string($actor_substr))."%' ORDER BY actor;";
		$result = mysql_query($query);
		if ($result == FALSE) {
			echo mysql_error()."<br>";
			exit;
		}
	}
		
	if ($movie_submitted) {
		$query = "SELECT id, CONCAT(`title`, '; ', `year`) AS cat FROM Movie WHERE `title` LIKE '%".sprintf("%s", mysql_real_escape_string($movie_substr))."%' ORDER BY cat;";
		$result = mysql_query($query);
		if ($result == FALSE) {
			echo mysql_error()."<br>";
			exit;
		}	
	}
?>

<?php if ($actor_submitted) :?>
	<?php if (mysql_num_rows($result) == 0) : ?>
		sorry, we could not find what you were looking for<br>
	<?php endif; ?>
	
	<?php while ($row = mysql_fetch_row($result)) : ?>
		<a href="actor_info.php?linked_aid=<?= $row[0] ?>"><?= $row[1] ?></a><br>
	<?php endwhile; ?>
<?php endif; ?>

<?php if ($movie_submitted) : ?>
	<?php if (mysql_num_rows($result) == 0) : ?>
		sorry, we could not find what you were looking for<br>
	<?php endif; ?>
	
	<?php while ($row = mysql_fetch_row($result)) :?>
		<a href="movie_info.php?linked_mid=<?= $row[0] ?>"><?= $row[1] ?></a><br> 
	<?php endwhile; ?>	
<?php endif; ?>

