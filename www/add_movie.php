<!DOCTYPE html>
<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

    if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<?php
    // the new id is the max id from Movie, + 1
	function get_new_id() {
		$get_max_query = "SELECT MAX(id) FROM Movie";
        $res = mysql_query($get_max_query);
        if (!empty($res)) {
			return mysql_result($res, 0) + 1;
        }
        else {
        	return 1;
        }
	}
?>

<FORM ACTION="movies_home.php">
    <INPUT TYPE="submit" VALUE="return to main menu"><br><br>
</FORM>

<FORM ACTION="add_movie.php" METHOD="post" ID="movie_form">
	<b>add new movie</b><br><br>
	title:
	<INPUT TYPE="text" NAME="title" SIZE=100 MAXLENGTH=100><br>
	
	year:
	<INPUT TYPE="text" NAME="year"><br>

	rating:
	<SELECT NAME="rating" FORM="movie_form">
		<OPTION SELECTED VALUE=""></OPTION>
		<OPTION VALUE="G">G</OPTION>
		<OPTION VALUE="NC-17">NC-17</OPTION>
		<OPTION VALUE="PG">PG</OPTION>
		<OPTION VALUE="PG-13">PG-13</OPTION>
		<OPTION VALUE="R">R</OPTION>
		<OPTION VALUE="Surrendered">Surrendered</OPTION>
	</SELECT>

	company:
	<INPUT TYPE="text" NAME="company" SIZE=50 MAXLENGTH=50><br>	
	
	genre(s):
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="action">action
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="adult">adult
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="adventure">adventure
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="animation">animation<br>
	&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="comedy">comedy
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="crime">crime
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="documentary">documentary
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="drama">drama<br>
	&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="family">family
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="fantasy">fantasy
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="horror">horror
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="musical">musical<br>
	&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="nystery">mystery
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="romance">romance
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="sci-fi">sci-fi
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="short">short<br>
	&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="thriller">thriller
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="war">war
	<INPUT TYPE="checkbox" NAME="genres[]" VALUE="western">western<br>

	<INPUT TYPE="submit" NAME="add_submit" VALUE="add">
	
</FORM>

<?php
	// save variabels from form
	$title = $_POST["title"];
	$year = $_POST["year"];
	$rating = $_POST["rating"];
	$company = $_POST["company"];
	
 	// if submit button, "add" is pressed
    if (isset($_POST["add_submit"])) {
		$new_id = get_new_id();
		$n_inserts = 0;

		// insert into Movie		
		$insert_cmd = sprintf( 
			"INSERT INTO Movie VALUES (%d, '%s', %d, '%s', '%s');",
			mysql_real_escape_string($new_id),
			mysql_real_escape_string($title),
			mysql_real_escape_string($year),
			mysql_real_escape_string($rating),
			mysql_real_escape_string($company) );

		if (!mysql_query($insert_cmd)) {
			echo mysql_error();
			exit;
		}

		// insert into MovieGenre
		foreach($_POST['genres'] as $genre) {
			$insert_cmd = sprintf(
				"INSERT INTO MovieGenre VALUES (%d, '%s');",
				mysql_real_escape_string($new_id),
				mysql_real_escape_string($genre) );
			if (!mysql_query($insert_cmd)) {
				echo mysql_error();
				exit;
			}				
		}
		echo "Movie inserted successfully"."<br>";
 	}
	else {
		echo "Press 'add' when you are ready."."<br>";
	}
?>




