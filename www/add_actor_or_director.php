<!DOCTYPE html>
<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

	if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<?php 
	$new_id = 1; 
	
	// the new id is the max id from Actor and Director, + 1
	function get_new_id() {
		$get_max_query = "SELECT MAX(id) FROM (SELECT id FROM Actor UNION SELECT id FROM Director) AS S";
		$res = mysql_query($get_max_query);		
		
		if (!empty($res)) {
			return mysql_result($res, 0) + 1;
		}
		else {
			return 1;
		}
	}
?>
<FORM ACTION="movies_home.php">
	<INPUT TYPE="submit" VALUE="return to main menu"><br><br>
</FORM>

<FORM ACTION="add_actor_or_director.php" METHOD="post">
	<b>add new actor and/or director</b><br><br>
	profession(s):
	<INPUT TYPE="checkbox" NAME="profession1" VALUE="actor"> actor    
	<INPUT TYPE="checkbox" NAME="profession2" VALUE="director"> director<br>    
	first name:
    <INPUT TYPE="text" NAME="first" SIZE=20 MAXLENGTH=20><br>     
    last name:
    <INPUT TYPE="text" NAME="last"  SIZE=20 MAXLENGTH=20> <br>
	sex:<br>
    <INPUT TYPE="radio" NAME="sex" VALUE="Male" CHECKED> male    
	<INPUT TYPE="radio" NAME="sex" VALUE="Female"> female<br>
	date of birth (YYYY-MM-DD):
	<INPUT TYPE="text" NAME="dob" SIZE=20 MAXLENGTH=20><br>
	date of death (YYYY-MM-DD):
	<INPUT TYPE="text" NAME="dod" SIZE=20 MAXLENGTH=20> 
    (leave blank if not dead)<br><br>	


	<INPUT TYPE="submit" NAME="add_submit" VALUE="add">

</FORM>

<?php 
	// save variables from form
	$profession1 = $_POST["profession1"];
	$profession2 = $_POST["profession2"];
	$first = $_POST["first"];
	$last = $_POST["last"];
	$sex = $_POST["sex"];
	$dob = $_POST['dob'];
	$dod = $_POST["dod"];

	// if submit button, "add" is pressed
	if (isset($_POST["add_submit"])) {
		if (empty($profession1) && empty($profession2)) {
			echo 'ERROR: you must select at least one profession'."<br>";
       		exit;
		}
		if (strcmp($profession1, "actor") == 0 
			&& strcmp($profession2, "director") != 0) {
			// insert into Actor table
			$insert_cmd = sprintf( "INSERT INTO Actor VALUES (%d, '%s', '%s', '%s', '%s', '%s');", 
			mysql_real_escape_string(get_new_id()), 
			mysql_real_escape_string($last),
			mysql_real_escape_string($first),
			mysql_real_escape_string($sex),
			mysql_real_escape_string($dob), 
			mysql_real_escape_string($dod)) ;
		
			if (!mysql_query($insert_cmd)) {
				echo "ERROR: MySQL failed to insert"."<br>";
				echo mysql_error();
				exit;	
			} else {
				echo "New actor inserted"."<br>"; 
			}
		}

		else if (strcmp($profession2, "director") == 0
			&& strcmp($profession1, "actor") != 0) {
			// insert into Director table		
			$insert_cmd = sprintf( "INSERT INTO Director VALUES (%d, '%s', '%s', '%s', '%s');", 
			mysql_real_escape_string(get_new_id()), 
			mysql_real_escape_string($last),
			mysql_real_escape_string($first),
			mysql_real_escape_string($dob),
			mysql_real_escape_string($dod) );

			if (!mysql_query($insert_cmd)) {
				echo "ERROR: MySQL failed to insert"."<br>";
				echo mysql_error();
				exit;	
			} else {
				echo "New director inserted"."<br>"; 
			}
		}
		else {// if both actor and director are checked
			// insert into both Actor and Director tables
			
			$new_id = get_new_id();
			$num_inserts = 0;

			$insert_cmd = sprintf( "INSERT INTO Actor VALUES (%d, '%s', '%s', '%s', '%s', '%s');", 
			mysql_real_escape_string($new_id), 
			mysql_real_escape_string($last),
			mysql_real_escape_string($first),
			mysql_real_escape_string($sex),
			mysql_real_escape_string($dob),
			mysql_real_escape_string($dod) );

			if (!mysql_query($insert_cmd)) {
				echo "ERROR: MySQL failed to insert"."<br>";
				echo mysql_error();
				exit;	
			} else {
				$num_inserts = $num_inserts + 1;
			}

			$insert_cmd = sprintf( "INSERT INTO Director VALUES (%d, '%s', '%s', '%s', '%s');", 
			mysql_real_escape_string($new_id), 
			mysql_real_escape_string($last),
			mysql_real_escape_string($first),
			mysql_real_escape_string($dob),
			mysql_real_escape_string($dod) );		
			
			if (!mysql_query($insert_cmd)) {
				echo "ERROR: MySQL failed to insert"."<br>";
				echo mysql_error();
				exit;	
			} else {
				$num_inserts = $num_inserts + 1;	
			}			
			
			if ($num_inserts == 2)
				echo "New actor/director inserted"."<br>";			 
		}
	}
	else {
		// TESTING
		echo "Press 'add' when you are ready."."<br>";
	}
?>
 
