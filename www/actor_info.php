<!DOCTYPE html>
<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

    if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<FORM ACTION="movies_home.php">
    <INPUT TYPE="submit" VALUE="return to main menu"><br><br>
</FORM>

<b>show actor info</b><br><br>

<?php
    $actor_query = "SELECT id, CONCAT(`first`, ' ', `last`, '; ', `dob`) AS cat FROM Actor ORDER BY cat;";
    $actor_result = mysql_query($actor_query);
?>

<FORM ACTION="actor_info.php" METHOD="post">
	actor:
	 <SELECT NAME="actor">
        <OPTION SELECTED VALUE=""></OPTION>
    <?php while ($row = mysql_fetch_row($actor_result)) : ?>
        <OPTION VALUE="<?= $row[0] ?>"><?= $row[1] ?></OPTION>
    <?php endwhile ?>
    </SELECT><br>
	
	<INPUT TYPE="submit" NAME="submit" VALUE="show info"><br><br>

</FORM>

<?php
	// save aid from form
	$aid = $_POST['actor'];
	
	// get aid from search link if available
	if (isset($_GET['linked_aid'])) {
		$aid = $_GET['linked_aid'];
	}
	
	if (isset($aid)) {
		// get actor name
		$name_result = mysql_query(sprintf("SELECT CONCAT(`first`, ' ', `last`) FROM Actor WHERE id = %d", mysql_real_escape_string($aid)));
		$row = mysql_fetch_row($name_result);
		echo "<b>actor: </b>".$row[0]."<br>";

		// select all movies that the selected actor played a role in
		$select_cmd = sprintf(
			"SELECT CONCAT(title, '; ', year), role FROM MovieActor, Movie WHERE mid = id AND aid = %d;",
			mysql_real_escape_string($aid) );
		$result = mysql_query($select_cmd);
		if ($result == FALSE) {
			echo mysql_error();
			exit;
		}	
		// display result
		while ($row = mysql_fetch_row($result)) {
			echo "<b>movie: </b>".$row[0]."; <b>role: </b>".$row[1];
			echo "<br>";
		}	 		
	}
	else {
		echo "press 'show info' when ready"."<br>";
	}
?>
