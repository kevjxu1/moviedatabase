<!DOCTYPE html>
<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

    if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<FORM ACTION="movies_home.php">
    <INPUT TYPE="submit" VALUE="return to main menu"><br><br>
</FORM>

<b>add director to movie</b><br><br>

<?php
    $director_query = "SELECT id, CONCAT(`first`, ' ', `last`, '; ', `dob`) AS cat FROM Director ORDER BY cat;";
    $director_result = mysql_query($director_query);
    $movie_query = "SELECT id, CONCAT(title, '; ', year) AS cat FROM Movie ORDER BY cat;";
    $movie_result = mysql_query($movie_query);
?>

<FORM ACTION="add_director_to_movie.php" METHOD="post" ID="director_to_movie_form">
    director:
    <SELECT NAME="actor" FORM="director_to_movie_form">
        <OPTION SELECTED VALUE=""></OPTION>
    <?php while ($row = mysql_fetch_row($director_result)) : ?>
        <OPTION VALUE="<?= $row[0] ?>"><?= $row[1] ?></OPTION>
    <?php endwhile ?>
    </SELECT><br>

    movie:
    <SELECT NAME="movie" FORM="director_to_movie_form">
        <OPTION SELECTED VALUE=""></OPTION>
    <?php while ($row = mysql_fetch_row($movie_result)) : ?>
        <OPTION VALUE="<?= $row[0] ?>"><?= $row[1] ?></OPTION>
    <?php endwhile ?>
    </SELECT><br>

	<INPUT TYPE="submit" NAME="add_submit" VALUE="add">
	
</FORM>

<?php
    // save variables from form
    $did = $_POST['actor'];
    $mid = $_POST['movie'];

    if (isset($_POST['add_submit'])) {
        $insert_cmd = sprintf(
            "INSERT INTO MovieDirector VALUES (%d, %d);",
            mysql_real_escape_string($mid),
            mysql_real_escape_string($did) );
        if (!mysql_query($insert_cmd)) {
            echo mysql_error();
            exit;
        }
		echo "director added to movie successfully"."<br>";
    }
	else {
		echo "press 'add' when you are ready"."<br>";
	}
?>

