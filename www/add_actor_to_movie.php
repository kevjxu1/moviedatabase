<!DOCTYPE html>
<?php
   $HOST = "localhost";
   $USER = "cs143";
   $PW = "";
   $DATABASE = "CS143";

    if (!$db_connection = mysql_connect($HOST, $USER, $PW)) {
        echo 'ERROR: could not connect to MySQL host';
        exit;
    }
    if (!mysql_select_db($DATABASE, $db_connection)) {
        echo 'ERROR: could not select database';
        exit;
    }
?>

<FORM ACTION="movies_home.php">
    <INPUT TYPE="submit" VALUE="return to main menu"><br><br>
</FORM>

<b>add actor to movie</b><br><br>

<?php
	$actor_query = "SELECT id, CONCAT(`first`, ' ', `last`, '; ', `dob`) AS cat FROM Actor ORDER BY cat;";
	$actor_result = mysql_query($actor_query);	
	
	$movie_query = "SELECT id, CONCAT(title, '; ', year) AS cat FROM Movie ORDER BY cat;";
	$movie_result = mysql_query($movie_query);
?>


<FORM ACTION="add_actor_to_movie.php" METHOD="post" ID="actor_to_movie_form">
	actor:
	<SELECT NAME="actor" FORM="actor_to_movie_form">
		<OPTION SELECTED VALUE=""></OPTION>
	<?php while ($row = mysql_fetch_row($actor_result)) : ?>
		<OPTION VALUE="<?= $row[0] ?>"><?= $row[1] ?></OPTION>		
	<?php endwhile ?>
	</SELECT><br>
	
	movie:
	<SELECT NAME="movie" FORM="actor_to_movie_form">
		<OPTION SELECTED VALUE=""></OPTION>
	<?php while ($row = mysql_fetch_row($movie_result)) : ?>
		<OPTION VALUE="<?= $row[0] ?>"><?= $row[1] ?></OPTION>
	<?php endwhile ?>
	</SELECT><br>
	
	role: 
	<INPUT TYPE="text" NAME="role" SIZE=50, MAXLENGTH=50><br>

	<INPUT TYPE="submit" NAME="add_submit" VALUE="add">
	
</FORM>

<?php
	// save variables from form
	$aid = $_POST['actor'];
	$mid = $_POST['movie'];
	$role = $_POST['role'];

	if (isset($_POST['add_submit'])) {
		$insert_cmd = sprintf(
			"INSERT INTO MovieActor VALUES (%d, %d, '%s');",
			mysql_real_escape_string($mid),	
			mysql_real_escape_string($aid),	
			mysql_real_escape_string($role) );	
		if (!mysql_query($insert_cmd)) {
			echo mysql_error();
			exit;
		}
		echo "actor added to movie successfully<br>";
	}	
?>
	
