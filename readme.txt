We have 8 php web pages. movies_home.php is the home page and contains the 
search bars for actors and movies. actor_info.php and movie_info.php display 
the info for a specifc actor or movie. The add... php files are web 
interfaces to add something to the database.

The main menu has a link to every page, and every page has a link to the 
main menu. Also in the main menu, one can search for an actor or movie, 
and display links to all actors or movies that contain the search submission 
as a substring. 
Each add page uses a form submission to add to the database. 
The word limit constraints are in check with the forms.

In the actor-info page, one can view all the movies that a particular actor 
played in, along with his or her role.

In the movie-info page, one can view all the actors and directors of a 
particular movie. We can also see all user comments along with the timestamp, 
author, and ratings of each review. We can see the average score of a 
particular movie. We also have a button that allows one to add a comment 
in the add-comment page.
